## Provision infrastructure
### requirements
* terraform cli
* aws exported credentials (via env variable or credentials file)

### provision
```
> cd terraform
> terraform init
> terraform plan -var-file=chorus.tfvars
> terraform apply -var-file=chorus.tfvars
```  

### termination
```
> terraform destroy -var-file=chorus.tfvars
```  

## Redis Provision
### requirements
* ansible + ansible-galaxy
* python
* aws exported credentials (via env variable or credentials file)
* terraform provisioned instance

### install dependencies
```
> cd ansible
> bash install_ansible_dependencies.sh
```  

### provision
```
> cd ansible
> bash provision_redis.sh
```  

## Deploy/Update API
### requirements
* ansible + ansible-galaxy
* python
* aws exported credentials (via env variable or credentials file)
* terraform provisioned instance
* source code in git

### install dependencies
```
> cd ansible
> bash install_ansible_dependencies.sh
```  

### provision
```
> cd ansible
> bash deploy_api.sh
```
