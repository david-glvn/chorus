from falcon import testing
import pytest

import main


# Depending on your testing strategy and how your application
# manages state, you may be able to broaden the fixture scope
# beyond the default 'function' scope used in this example.

@pytest.fixture()
def client():
    # Assume the hypothetical `myapp` package has a function called
    # `create()` to initialize and return a `falcon.API` instance.
    return testing.TestClient(main.create())


def test_healtcheck(client):
    doc = {u'api_ok': True, u'redis_ok': True}

    result = client.simulate_get('/v1/health')
    assert result.json == doc


def test_upper_case(client):
    doc = 'TEST'

    result = client.simulate_get('/v1/upper/test')
    assert result.text == doc


def test_lower_case(client):
    doc = 'test'

    result = client.simulate_get('/v1/lower/TEST')
    assert result.text == doc
