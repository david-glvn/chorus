import falcon
from wsgiref import simple_server

from utils import api
from utils import log
from utils import config

LOG = log.get_logger()


class ChrousAPI(falcon.API):
    """Render Chorus Exam API Class."""

    def __init__(self, *args, **kwargs):
        """Render API routes."""
        super(ChrousAPI, self).__init__(*args, **kwargs)

        LOG.info('API Server is starting')
        self.add_route('/v1/upper/{input_string}', api.StringResource())
        self.add_route('/v1/lower/{input_string}', api.StringResource())
        self.add_route('/v1/health', api.HealthCheck())


def create():
    return ChrousAPI()


if __name__ == "__main__":
    application = create()
    httpd = simple_server.make_server(config.api_listener, config.api_port,
                                      application)
    httpd.serve_forever()
