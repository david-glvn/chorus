from os import getenv as e

api_listener = e('CHORUS_API_LISTENER', '0.0.0.0')
try:
    api_port = int(e('CHORUS_API_PORT', '8002'))
except ValueError:
    print('Chorus API port must be integer')
    exit(1)

log_level = e('CHORUS_LOG_LEVEL', 'INFO')

redis_host = e('CHORUS_REDIS_HOST', 'localhost')
try:
    redis_port = int(e('CHORUS_REDIS_PORT', '6379'))
except ValueError:
    print('Chorus Redis port must be integer')
    exit(1)
try:
    redis_db = int(e('CHORUS_REDIS_DB', '0'))
except ValueError:
    print('Chorus Redis DB must be integer')
    exit(1)
try:
    redis_list_retention = int(e('CHORUS_REDIS_RETENTION', '3'))
except ValueError:
    print('Chorus Redis Retention must be integer')
    exit(1)
