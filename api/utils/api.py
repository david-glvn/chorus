import json
import redis
import falcon

from utils import log
from utils import config


LOG = log.get_logger()
redis_db = redis.StrictRedis(host=config.redis_host,
                             port=config.redis_port,
                             db=config.redis_db)


class HealthCheck(object):
    """Simple healthcheck."""

    def on_get(self, req, resp):
        """Check if API is healthy.

        Returns
        -------
        JSON with status of relevant components

        """
        try:
            redis_status = redis_db.ping()
        except redis.RedisError:
            redis_status = False
        resp.status = falcon.HTTP_200
        resp.body = json.dumps({'api_ok': True,
                                'redis_ok': redis_status})


class StringResource(object):
    """Manipulate string requests."""

    def on_get(self, req, resp, input_string):
        """Return sting in lower or upper case.

        Parameters
        ----------
        input_string : str
            Requested string

        Returns
        -------
        Requested string in all lower or upper case

        """
        string_case = req.path.split('/')[2]
        if string_case == 'upper':
            LOG.info('Turning "{}" into upper case'.format(input_string))
            return_string = input_string.upper()
        elif string_case == 'lower':
            LOG.info('Turning "{}" into lower case'.format(input_string))
            return_string = input_string.lower()
        else:
            raise falcon.HTTPError(falcon.HTTP_400,
                                   'Unknown action',
                                   'You have requested unknown action: {}'.
                                   format(string_case))

        insert_to_redis(string_case, input_string)

        resp.status = falcon.HTTP_200
        resp.body = return_string


def insert_to_redis(list, value):
    LOG.info('Adding "{}" to `{}` list'.format(value, list))
    try:
        if redis_db.llen(list) >= config.redis_list_retention:
            LOG.info('Removing first key from the list: `{}`'.format(list))
            redis_db.ltrim(list, 1, -1)
    except redis.RedisError:
        LOG.error('Something went wrong with redis removal from index')
        raise falcon.HTTPError(falcon.HTTP_500,
                               'Server Error',
                               'Something has broken on our side, we are \
                               fixing it now')
    try:
        redis_db.rpush(list, value)
    except redis.RedisError:
        LOG.error('Something went wrong with redis strore to index')
        raise falcon.HTTPError(falcon.HTTP_500,
                               'Server Error',
                               'Something has broken on our side, we are \
                               fixing it now')
