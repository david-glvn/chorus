provider "aws" {
  region = "${var.region}"
}

data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name = "name"

    values = [
      "amzn-ami-*-amazon-ecs-optimized"
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.vpc_name}"
  cidr = "${var.vpc_cidr}"

  azs             = "${var.vpc_azs}"
  public_subnets  = "${var.vpc_public_subnets}"

  enable_nat_gateway = "${var.enable_nat_gateway}"
  enable_vpn_gateway = "${var.enable_vpn_gateway}"

  tags = {
    Terraform = "true"
    Environment = "${var.vpc_name}"
  }
}

module "public_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "test-davidg-public"
  description = "Security group for test"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_rules            = ["all-icmp"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 8002
      to_port     = 8002
      protocol    = "tcp"
      description = "Chorus API"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "SSH"
      cidr_blocks = "77.124.115.122/32"
    }
  ]

  egress_rules = ["all-all"]
}

module "redis_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "test-davidg-redis"
  description = "Security group for redis test"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress_with_cidr_blocks = [
    {
      from_port   = 6379
      to_port     = 6379
      protocol    = "tcp"
      description = "Redis"
      cidr_blocks = "${var.vpc_cidr}"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "SSH"
      cidr_blocks = "77.124.115.122/32"
    }
  ]

  egress_rules = ["all-all"]
}

module "api" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name           = "api"
  instance_count = 1

  ami                         = "${data.aws_ami.amazon_linux.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${module.public_security_group.this_security_group_id}"]
  subnet_id                   = "${element(module.vpc.public_subnets, 0)}"
  associate_public_ip_address = "${var.api_associate_public_ip_address}"

  tags = {
    Component = "api"
    Terraform = "true"
    Environment = "${var.vpc_name}"
  }
}

module "redis" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name           = "redis"
  instance_count = 1

  ami                         = "${data.aws_ami.amazon_linux.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${module.redis_security_group.this_security_group_id}"]
  subnet_id                   = "${element(module.vpc.public_subnets, 0)}"
  associate_public_ip_address = "${var.redis_associate_public_ip_address}"

  tags = {
    Component = "redis"
    Terraform = "true"
    Environment = "${var.vpc_name}"
  }
}
